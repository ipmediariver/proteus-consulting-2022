@component('mail::message')
# New Driver Request

Full name: {{ $fullname }}

E-mail: {{ $email }}

Phone: {{ $phone }}

Areas of operation: {{ $areas_of_operation }}

Principles number: {{ $principles_number }}

Date: {{ $date }}

Medical conditions: {{ $medical_conditions }}


{{ config('app.name') }}
@endcomponent
