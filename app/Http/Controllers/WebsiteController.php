<?php

namespace App\Http\Controllers;

use App\Mail\ContactUs;
use App\Mail\DriverRequestConfirmation;
use App\Mail\NewDriverRequest;
use App\Mail\NewMessage;
use App\Models\Suscriptor;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Validator;

class WebsiteController extends Controller
{

    public function index()
    {
        return Inertia::render('Website/Index');
    }

    public function driverRequestForm(Request $request){


        $validationRules = [
            'fullname' => 'required|string',
            'email' => 'required|email',
            'phone' => 'nullable|string',
            'areas_of_operation' => 'required|string',
            'principles_number' => 'required|numeric',
            'date' => 'required|date_format:Y-m-d',
            'medical_conditions' => 'nullable|string' 
        ];

        $validate = Validator::make($request->all(), $validationRules)->validate();

        \Mail::to('info@proteusglobal.us')->send(new NewDriverRequest($request->all()));
        \Mail::to($request->email)->send(new DriverRequestConfirmation($request->all()));

        return redirect()->back()->with('requestSuccess', 'Your request has been sended.');


    }

    public function suscribeUser(Request $request)
    {

        $validate = Validator::make(
            $request->only(['name', 'email']),
            $this->suscriptionRules()
        )->validate();

        $suscriptor        = new Suscriptor();
        $suscriptor->name  = $request->name;
        $suscriptor->email = $request->email;
        $suscriptor->save();

        return redirect()->back()->with('suscriptorSuccess', "Thank you {$suscriptor->name} for registering with us, we will keep you updated.");

    }

    public function suscriptionRules()
    {
        return [
            'name'  => 'required|string',
            'email' => 'required|email|unique:suscriptors',
        ];
    }

    public function about(){
        return Inertia::render('Website/About');
    }

    public function service($service)
    {

        $serviceData = $this->getServiceData($service);
        return Inertia::render('Website/Service', ['service' => $serviceData]);

    }

    public function getServiceData($service)
    {
        if ($service == 'consulting-services') {
            return [
                'bgimg'       => '/img/sean-pollock-PhYq704ffdA-unsplash.jpg',
                'title'       => 'Consulting Services',
                'description' => 'The most effective security programs are those that are based on preventive strategies and infrastructures, rather than waiting for the problem to occur and then taking corrective measures.',
                'body'        => '
                    <p>Crime Intelligence Mapping (MEXICO)</p>
                    <p>Risk/Threat Assessment</p>
                    <p>Complete Security Programs</p>
                    <p>CSO Outsourcing</p>
                    <p>Augmented Due Diligence</p>
                    <p>Loss Prevention Programs</p>
                    <p>Augmented CCTV & Access Control</p>
                    <p>Disaster Recovery/ EVAC Programs</p>
                    <p>Medical Emergency & Contingency</p>
                ',
            ];
        } elseif ($service == 'private-transportation-for-executives-and-key-personnel') { 
           return [
                'bgimg'       => '/img/private-transportation-2.jpeg',
                'title'       => 'PRIVATE TRANSPORTATION FOR EXECUTIVES AND KEY PERSONNEL',
                'description' => '<p class="mb-4">IT IS MORE THAN A CAR SERVICE</p><p class="mb-4">WE TRANSPORT YOUR EXECUTIVES AND KEY EMPLOYEES TO MEXICO AND BACK.</p><p>WE ARE YOUR SAFE TRANSPORTATION PARTNER.</p>',
                'body'        => '',
            ];
        }
    }

    public function contact(){
        return Inertia::render('Website/Contact');
    }
    public function submitContact(Request $request){

        $rules = [
            'fullName' => 'required|string',
            'email' => 'required|email',
            'company' => 'nullable|string',
            'phone' => 'nullable|string',
            'subject' => 'required|string',
            'message' => 'required|string'
        ];

        $validate = Validator::make($request->all(), $rules)->validate();

        \Mail::to($request->email)->send(new ContactUs($request->all()));
        \Mail::to('info@proteusglobal.us')->send(new NewMessage($request->all()));

        return redirect()->back()->with('message', "Thank you for contacting us, we will reply to you as soon as possible.");

    }

}
