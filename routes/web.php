<?php
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\WebsiteController;

Route::get('/', [WebsiteController::class, 'index'])
    ->name('home');

Route::post('/suscribe-user-form', [WebsiteController::class, 'suscribeUser'])->name('suscribe-user-form');

Route::get('/services/{service}', [WebsiteController::class, 'service'])->name('service');
Route::get('/about-us', [WebsiteController::class, 'about'])->name('about-us');
Route::get('/contact-us', [WebsiteController::class, 'contact'])->name('contact');
Route::post('/contact-us', [WebsiteController::class, 'submitContact'])->name('submit-contact');
Route::post('/driver-request-form', [WebsiteController::class, 'driverRequestForm'])->name('driver-request-form');


Route::get('/mailable', function () {

    $data = [
        'fullName' => 'Full Name',
        'email' => 'test@test.com',
        'subject' => 'Lorem ipsum ad his scripta',
        'message' => 'Lorem ipsum ad his stripta blandit partiendo adipsing consectur sit amet',
        'company' => 'Company Name',
        'phone' => '(000) 000-0000'
    ];
    return new App\Mail\NewMessage($data);
});


Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');
require __DIR__.'/auth.php';