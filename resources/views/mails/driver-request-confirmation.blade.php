@component('mail::message')
# Your request details

Thank you for requesting our executive transportation services, below we show you the details of your requirement.

Full name: {{ $fullname }}

E-mail: {{ $email }}

Phone: {{ $phone }}

Areas of operation: {{ $areas_of_operation }}

Principles number: {{ $principles_number }}

Date: {{ $date }}

Medical conditions: {{ $medical_conditions }}


{{ config('app.name') }}
@endcomponent
