@component('mail::message')
# New message

Name: {{ $request['fullName'] }}

E-mail: {{ $request['email'] }}

Subject: {{ $request['subject'] }}

Message: {{ $request['message'] }}

Company: {{ $request['company'] }}

Phone: {{ $request['phone'] }}




{{ config('app.name') }}
@endcomponent
